using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Shoot : MonoBehaviour
{
    [SerializeField] float _Speed = 12f;
    [SerializeField] Dart _Dart;
    [SerializeField] Transform asd;
    Dart _Create;
    Vector2 _move;        
    Vector3 PX;
        Dart loadDart;
    float PlayerMove;
    float MinY = -5;
    float MaxY = 5;

    // Start is called before the first frame update
    void Start()
    {
        PX = transform.position;
        preprare();
    }
    void preprare()
    {
                PX.x += 0.7f;
        loadDart=Instantiate(_Dart, PX, Quaternion.identity,asd);
        
    }

    // Update is called once per frame
    private void Update()
    {
        PX = transform.position;
                float clampedY = Mathf.Clamp(transform.position.y, MinY, MaxY);
        transform.position = new Vector3(-5, clampedY, transform.position.z);
        transform.Translate(0, PlayerMove *_Speed *Time.deltaTime, 0);
    }
    void OnFire(InputValue value)
    {
        if(loadDart != null)
        {
            loadDart.Shoot();
            loadDart = null;
            Invoke(nameof(preprare),1);
            
        }
        
    }
    void OnMove(InputValue value)
    {
        _move = value.Get<Vector2>();
        PlayerMove = _move.y;
    }

}
