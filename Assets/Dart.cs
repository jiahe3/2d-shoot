using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Dart : MonoBehaviour
{
    [SerializeField] float _ShootSpeed = 150f;
    // Start is called before the first frame update
    public void Shoot()
    {
        Rigidbody2D _rb = GetComponent<Rigidbody2D>();
        _rb.velocity = _ShootSpeed * Vector2.right ;
        transform.SetParent(null);
    }

    // Update is called once per frame
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<Dart>())
        {
            SceneManager.LoadScene("End1");
        }
        transform.SetParent(collision.transform);
        Rigidbody2D _rb = GetComponent<Rigidbody2D>();
        _rb.isKinematic = true;
        var ScoreC = FindObjectOfType<ScoreCounter>();
        ScoreC.GetScore();
    }
}
