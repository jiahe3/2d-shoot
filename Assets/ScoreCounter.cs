using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ScoreCounter : MonoBehaviour
{
    int Score;
    public void GetScore()
    {
        Score++;
        var text = GetComponent <TMP_Text>();
        text.SetText(Score.ToString());
        if(Score >= 5 )
        {
            SceneManager.LoadScene("End 1");
        }
    }
}
