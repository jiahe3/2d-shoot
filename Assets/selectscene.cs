using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class selectscene : MonoBehaviour
{
    string Select;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Select = collision.gameObject.name;
        if(Select == "Normal")
        {
            SceneManager.LoadScene("Normal");
        }
        else if(Select == "Easy")
        {
            SceneManager.LoadScene("Easy");
        }
        else if (Select == "Hard")
        {
            SceneManager.LoadScene("Hard");
        }
    }
}
